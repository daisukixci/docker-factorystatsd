FROM python:3.10-alpine

ENV SCRIPT_OUTPUT="/mnt"
ENV STATSD_IP="localhost"
ENV STATSD_PORT="8125"
ENV VERSION="0.1.1"
ENV WORKDIR="/"

#RUN adduser -Ds /bin/sh factorystats

WORKDIR $WORKDIR

COPY entrypoint.sh ./
ADD https://github.com/ccbrown/factorystatsd/releases/download/v${VERSION}/factorystatsd.zip ./
RUN unzip ./factorystatsd.zip
RUN ln -s $WORKDIR/factorystatsd-0.1.1 $WORKDIR/factorystatsd

#USER factorystats

ENTRYPOINT ["./entrypoint.sh"]
CMD ["run"]
