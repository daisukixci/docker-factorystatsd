# Docker Factorystatsd
Run the https://github.com/ccbrown/factorystatsd forwarder python script inside a docker container

## Run
```bash
docker run --rm -it -v /PATH/TO/script-output:/mnt/script-output daisukixci/factorystatsd
```
