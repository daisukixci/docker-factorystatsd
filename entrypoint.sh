#!/usr/bin/env sh

if [ "$1" == "run" ]; then
    exec python ./factorystatsd/forwarder.py \
        --factorio-script-output ${SCRIPT_OUTPUT}/script-output \
        --statsd-flavor dogstatsd \
        --statsd-host ${STATSD_IP} \
        --statsd-port ${STATSD_PORT}
else
    exec "$@"
fi
